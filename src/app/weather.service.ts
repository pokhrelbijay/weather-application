import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class WeatherService {

  apiKey= '0a56341512eed29e844de54faf5571e2';
  url: string;

  
  constructor(public http: Http) {
    this.url = 'http://api.openweathermap.org/data/2.5/forecast?q=';

  }


  getWeather(city, code) {
    return this.http.get(this.url + city + "&units=metric"+'&APPID=' + this.apiKey).map( res => res.json());

  }


}
